import Vue from "vue";
import VueRouter from "vue-router";
import Dashboard from "./pages/Dashboard";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Usuarios from "./pages/Usuarios";
import Crear from "./components/Usuarios/Crear";
import Editar from "./components/Usuarios/Editar";
import Materias from "./pages/Materias";
import MCrear from "./components/Materias/Crear";
import MEditar from "./components/Materias/Editar";
import Alumnos from "./pages/Alumnos";
import ACrear from "./components/Alumnos/Crear";
import AEditar from "./components/Alumnos/Editar";
import Carreras from './pages/Carreras';
import CCrear from './components/Carreras/Crear';
import MiCuenta from './pages/MiCuenta';
import MisMaterias from'./components/Alumnos/MisMaterias'
import Calificar from './components/Alumnos/Calificar'

Vue.use(VueRouter);

export const routes = [
  {
    path: "/", name: 'login', component: Login
  },
  {
    path: "/cesba", name: 'home', component: Home,
    children: [
      { path: "/inicio", name: 'inicio', component: Dashboard },
      { path: "/usuarios", name: 'usuarios', component: Usuarios },
      { path: "/usuarios/agregar", name: 'agregarUsuario', component: Crear },
        // Carreras
      { path: "/carreras", name: 'carreras', component: Carreras },
      { path: "/carreras/agregar", name: 'agregarCarrera', component: CCrear },
      
       // Materias
      { path: "/materias", name:'materias', component: Materias},
      { path: "/materias/agregar", name: 'agregarMateria', component: MCrear },
      { path: "/materias/editar/:id", name:'editarMateria', component: MEditar },
      { path: "/misMaterias/", name:'misMaterias', component: MisMaterias },
      { path: "/materias/alumno/:id", name: 'calificarAlumno', component: Calificar },
      
          //Alumnos
      { path: "/alumnos", name:'alumnos', component:  Alumnos},
      { path: "/alumnos/agregar", name:'agregarAlumno', component: ACrear },
      { path: "/alumnos/editar/:id", name: 'editarAlumno', component: AEditar },
      { path: "/miCuenta", name: 'miCuenta', component: MiCuenta },
    ]
  },

];
