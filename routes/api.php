<?php

use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->group(function(){

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::get('checkToken', 'AuthController@checkToken');  
});
Route::get('users', 'UserController@index');
Route::post('user', 'UserController@store');
Route::get('roles', 'UserController@getRoles');
Route::resource('carrera', 'CarreraController')->except(['edit', 'create']);
Route::get('carreraActiva', 'CarreraController@carrerasActivas');
Route::resource('materia', 'MateriaController')->except(['edit','destroy', 'create']);
Route::get('misMaterias','MateriaController@misMaterias');
Route::get('materias/calificar/{id}','MateriaController@materiasAlumno');
Route::post('materias/calificar','MateriaController@editarMateria');
Route::resource('alumno', 'AlumnoController')->except(['edit','destroy', 'create']);
Route::get('descargarPDF', 'AlumnoController@descargarPDF');
