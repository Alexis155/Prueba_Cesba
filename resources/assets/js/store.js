import Vuex from 'vuex';
import Vue from 'vue';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        token: localStorage.getItem('auth') || '',
        user: JSON.parse(localStorage.getItem('user')),
    },
    mutations:{
        setToken(state, token){
            localStorage.setItem('auth', token);
            localStorage.setItem('access', true);
            state.token = token;            
        },
        setUser(state, user){
            localStorage.setItem('user', JSON.stringify(user));
            state.user = user;
        },
        clearToken(state){
            localStorage.removeItem('auth');
            localStorage.removeItem('user');
            state.token = '';
            state.user = null 
        },        
    }
});