<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="csrf-token" value={{ csrf_token() }}>
  <title>CESBA</title>
  <link rel="stylesheet" href="{{ asset('css/all.min.css') }}">
  <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>
<body>
  <div id="app" class="wrapper">
    
  </div>
  <script src="{{ asset('js/jquery.min.js') }}"></script>
  {{-- <script src="{{ asset('js/jquery-ui.min.js') }}"></script> --}}
  <script src="{{ asset('js/bootstrap.min.js') }}"></script>
  <script src="{{ mix('js/app.js') }}"></script>
</body>
</html>