<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materia extends Model
{
    protected $fillable = [
        'nombre', 'codigo', 'grado', 'calificacion'
    ];
    public function carreras()
    {
        return $this->belongsToMany(Carrera::class,'carrera_materia');
    }
    public function alumnos()
    {
        return $this->belongsToMany(Alumno::class, 'kardexes')->withPivot(['calificacion_final','id']);
    }    
}
