<?php

namespace App\Http\Controllers;

use App\Carrera;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CarreraController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    
    public function index()
    {
        if(auth()->user()->role->nombre == 'Super Admin'){
            $carreras= Carrera::all();
        }else{
            $carreras = Carrera::where('estatus',0)->get();
        }
        return response()->json($carreras, 200);
    }

    public function carrerasActivas()
    {
        $carreras = Carrera::where('estatus',0)->get();
        return response()->json($carreras, 200);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nombre' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return response()->json($validator->errors()->all(), 200);
        }
        $carrera = Carrera::create([
            'nombre' => $request->nombre,
            'status' => 0
        ]);
        return response()->json('Se agrego correctamete la carrera');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $carrera = Carrera::find($id);
        return response()->json($carrera, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'nombre' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return response()->json($validator->errors()->all(), 200);
        }
        $carrera = Carrera::find($id);
        $carrera->update($request->all());
        return response()->json('Se modifico correctamente la carrera', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $carrera = Carrera::find($id);
        if($carrera->estatus == 0){
            $carrera->update([
                'estatus' => 1
            ]);
            return response()->json('La carrera se dio de baja', 200);
        }else{
            if(auth()->user()->role->nombre == 'Super Admin'){
                $carrera->update([
                    'estatus' => 0
                ]);
                return response()->json('La carrera se volvio a activar', 200);
            }
        }
    }
}
