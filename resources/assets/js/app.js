require("./bootstrap");
require("./../../../public/js/adminlte")
require("../../../public/js/all")

import Vue from "vue";
import App from "./pages/App";
import { routes } from "./router";
import VueRouter from "vue-router";
import VueAxios from "vue-axios";
import axios from "axios";
import { store } from "./store";
axios.defaults.headers.common = {
  'Authorization': 'Bearer ' + store.state.token
}
Vue.use(VueRouter);
Vue.use(VueAxios, axios);

const router = new VueRouter({
  routes,
  mode: "history",
});

const app = new Vue({
  el: "#app",
  router: router,
  store : store,
  render: (app) => app(App),
});
