<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kardex extends Model
{
    protected $fillable = [
        'alumno_id','calificacion_final'
    ];
    public function materias()
    {
        return $this->belongsToMany(Materia::class,'materia_kardex');
    }
    public function alumno()
    {
        return $this->hasOne(Alumno::class);
    }
}
