<?php

namespace App\Http\Controllers;

use App\Materia;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class MateriaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        $materias = Materia::all();
        return response()->json($materias, 200);
    }

    public function store(Request $request)
    {
       // dd(sizeof($request->carreras_id));
       $codigo = date('Y').date('n').date('d').rand(0, 1000);
        $rules = [
            'codigo' => 'required|unique:materias,codigo',
            'nombre' => 'required',
            'grado' => 'required',
            'calificacion' => 'required',
        ];
        $validator = Validator::make([
            'nombre' => $request->nombre,
            'codigo' => $codigo,
            'grado' => $request->grado,  
            'calificacion' => $request->calificacion   
        ], $rules);
        if($validator->fails()){
            return response()->json($validator->errors()->all(), 200);
        }
        
        $materia = Materia::create([
            'nombre' => $request->nombre,
            'codigo' => $codigo,
            'grado' => $request->grado,  
            'calificacion' => $request->calificacion
        ]);
        $materia->carreras()->attach($request->carreras_id);
        return response()->json([$materia], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Materia  $materia
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $materia = Materia::with('carreras')->where('id',$id)->first();
        return response()->json($materia);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Materia  $materia
     * @return \Illuminate\Http\Response
     */
    public function edit(Materia $materia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Materia  $materia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $materia = Materia::find($id);
        $rules = [
            'nombre' => 'required',
            'grado' => 'required',
            'calificacion' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return response()->json($validator->errors()->all(), 200);
        }
        $materia->update([
            'nombre' => $request->nombre,
            'grado' => $request->grado,  
            'calificacion' => $request->calificacion
        ]);
        return response()->json('Se edito correctamente', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Materia  $materia
     * @return \Illuminate\Http\Response
     */
    public function destroy(Materia $materia)
    {
        //
    }

    public function misMaterias(){
        $alumno_id = auth()->user()->alumno->id;    
        $misMaterias = Materia::with('alumnos')->whereHas('alumnos',function(Builder $q) use($alumno_id){
            $q->where('alumnos.id', '=', $alumno_id);
        })->get();
        return response()->json($misMaterias,200);
    }

    public function materiasAlumno($id){

        $materias = Materia::with('alumnos')->whereHas('alumnos', function(Builder $q) use($id) {
            $q->where('alumnos.id',$id);
        })->get();        
        return response()->json($materias, 200);
    }
    public function editarMateria(Request $request){
        DB::update('update kardexes set calificacion_final = ? where id = ?', [$request->calificacion_final, $request->kardex_id]);
        return response()->json(['status'=>true],200);
    }
}
