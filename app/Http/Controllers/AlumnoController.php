<?php

namespace App\Http\Controllers;

use App\Alumno;
use App\Kardex;
use App\Materia;
use App\Role;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Barryvdh\DomPDF\Facade as PDF;

class AlumnoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    
    public function index()
    {
        $alumnos = Alumno::with(['user','carrera'])->get();
        return response()->json($alumnos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $matricula = date('Y').date('n').date('d').rand(0, 1000);;           
        $rules = [
            'matricula' => 'required|unique:alumnos,matricula',
            'nombres' => 'required',
            'apellidos' => 'required',
            'grado' => 'required',
            'carrera_id' => 'required'

        ];        
        $validator = Validator::make([
            'matricula' => $matricula,
            'nombres' => $request->nombres,
            'apellidos' => $request->apellidos,
            'grado' => $request->grado,
            'carrera_id' => $request->carrera_id
        ], $rules);
        if($validator->fails()){
            return response()->json($validator->errors()->all(), 200);
        }
        $role = Role::where('nombre', 'Estudiante')->first();
        $user = User::create([
            'name' => $request->nombres,
            'email' => $matricula.'@cesba.edu.mx',
            'password' => bcrypt('tempo01'), //default password
            'role_id' => $role->id,
        ]);  
        $alumno = Alumno::create([
            'matricula' => $matricula,
            'nombres' => $request->nombres,
            'apellidos' => $request->apellidos,
            'grado' => $request->grado,
            'carrera_id' => $request->carrera_id,
            'user_id' => $user->id
        ]);        
        $this->asignarMaterias($request->carrera_id, $alumno->grado, $alumno);
        return response()->json(["Correcto"], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Alumno  $alumno
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $alumno = Alumno::find($id);
        return response()->json($alumno);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Alumno  $alumno
     * @return \Illuminate\Http\Response
     */
    public function edit(Alumno $alumno)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Alumno  $alumno
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'nombres' => 'required',
            'apellidos' => 'required',
            'grado' => 'required',
            'carrera' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return response()->json($validator->errors()->all(), 200);
        }
        $alumno = Alumno::find($id);
        $alumno->update([
            'nombres' => $request->nombres,
            'apellidos' => $request->apellidos,
            'grado' => $request->grado,
            'carrera' => $request->carrera,
        ]);
        return response()->json('Se modifico el alumno',200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Alumno  $alumno
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $alumno = Alumno::find($id);
        $alumno->update([
            'status' => 2
        ]);
        return response()->json('Se de dio de baja', 200);
    }

    private function asignarMaterias($carrera_id, $grado, $alumno){
        $materias = Materia::whereHas('carreras', function(Builder $query) use($carrera_id){
            $query->where('carreras.id','=',$carrera_id);
        })->where('materias.grado', $grado)->get();
        $alumno->materias()->attach($materias);
    }

    public function descargarPDF(){
        $alumno_id = auth()->user()->alumno->id; 
        $materias = Materia::with('alumnos')->whereHas('alumnos',function(Builder $q) use($alumno_id){
            $q->where('alumnos.id', '=', $alumno_id);
        })->get();
        $pdf = PDF::loadView('kardex', compact('materias'));
         return response()->json( $pdf->download('kardex'.auth()->user()->alumno->nombre.'.pdf'), 200);
    }
}
