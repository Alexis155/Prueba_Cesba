<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alumno extends Model
{
    protected $fillable = [
        'matricula', 'nombres', 'apellidos', 'grado', 'carrera_id', 'user_id'
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function carrera()
    {
        return $this->belongsTo(Carrera::class);
    }
    public function materias()
    {
        return $this->belongsToMany(Materia::class,'kardexes');
    }
}
