<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = Role::where('nombre', 'Super Admin')->first();
        $role_user = Role::where('nombre', 'Administrador')->first();
        $users = [
            [
                'user' => [
                    'name' => 'kevin',
                    'email' =>'kevin@gmail.com',
                    'password' => '12345678',
                    'role_id' => $role_admin->id
                ]
            ]
                ];
        foreach($users as $user){
            $user['user']['password'] = bcrypt($user['user']['password']);
            $new_user = User::create($user['user']);
        }
    }
}
