<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            'Super Admin',
            'Admin',
            'Estudiante'
        ];
        foreach($roles as $role){
            Role::create(['nombre' => $role]);
            
        }
    }
}
